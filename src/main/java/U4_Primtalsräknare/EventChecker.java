package U4_Primtalsräknare;

// "EventChecker"-klassen implementerar interfacet "Runnable"
class EventChecker implements Runnable {
    int start;
    int end;
    int count;

    public EventChecker(int start, int end, int count) {
        this.start = start;
        this.end = end;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    // Metoden "CountUp()" håller och räknar upp int-variabeln "count" med 1 ("++").
    private synchronized void CountUp() {
        count++;
    }

    //PrimtalsTest()-metoden testar om ett tal, "x" är ett primtal.
    public static boolean primtalsTest(int x) {

        if (x < 2) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(x); i++) {
            if (x % i == 0) {
                return false;
            }
        }
        return true;
    }

    // run()-metoden kör for-loop med metoden "primtalsTest()" och "CountUp()" (via inbyggt Functional interface "Runnable").
    @Override
    public void run() {
        for (int i = start; i <= end; i++) {
            if (primtalsTest(i)) {
                CountUp();
            }
        }
    }
}