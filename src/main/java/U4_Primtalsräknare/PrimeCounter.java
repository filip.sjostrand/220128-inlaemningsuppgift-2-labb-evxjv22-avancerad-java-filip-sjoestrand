package U4_Primtalsräknare;

// 4.	Räkna ut antalet primtal inom intervallet 0 till 500'000.
// Dela upp intervallet på 2 eller flera trådar,
// som var för sig räknar på antalet primtal inom sin tilldelade del parallellt.
// Du kan t.ex. låta en tråd ta en första del (typ 0 till 350'000) och en annan tråd resten  350'001 till 500'000.
// Det är dock givetvis tillåtet med något eget mer avancerat och effektivt upplägg också.


public class PrimeCounter {

    public static void main(String[] args) throws InterruptedException {

        // Objekten "eventchecker1" & "eventchecker2" tilldelas följande argument:
        EventChecker eventchecker1 = new EventChecker(0, 350000, 0);
        EventChecker eventchecker2 = new EventChecker(350001, 500000, 0);

        // Objekten "eventchecker1" & "eventchecker2" läggs in som parametrar i två separata trådar "thread1" & "thread2"
        // start-, end- samt count-värde för objekten skickas in i "run()" (se EventChecker-klassen)-
        Thread thread1 = new Thread(eventchecker1);
        Thread thread2 = new Thread(eventchecker2);

        // Ett valt primtal testas (egenkontroll)
        System.out.println(EventChecker.primtalsTest(169));
        // Trådarna startas
        thread1.start();
        thread2.start();
        // Tråd 1 & 2 inväntar varandra
        thread1.join();
        thread2.join();
        // När båda trådarna har körts klart presenteras count-värdena via "getCount()-metoden"
        System.out.println(eventchecker1.getCount() + eventchecker2.getCount());

    }
}