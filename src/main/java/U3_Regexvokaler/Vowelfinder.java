package U3_Regexvokaler;

//3.	Skapa en lista av ord. Använd reguljära uttryck för att
//      plocka ut endast de ord som innehåller
//      2 eller fler engelska vokaler (a, e, i, o, u, y)

import java.util.regex.MatchResult;
import java.util.regex.Pattern;

public class Vowelfinder {

    public static void main(String[] args) {

        // Regular Expression ("Regex"), "vowelpattern" sätts till följande:
        Pattern vowelpattern = Pattern.compile("\\b(?i)[^\\W]*[aeiouy]{1,}[^\\Waeiouy]*[aeiouy]{1,}[^\\W]*\\b");

        // Sträng att testa
        String testString = "k i y o u no on kak AKK kkffsddd ooooooyyyy aokaaaeou fffaaeevv pineapple aanjee ananas iiooo  okko been ieoa AAA oko ook moo noo ae oo";

        // Matcha test-sträng ("testString") på regex ("vowelpattern") och skriv ut resultatet på matchningen.
        vowelpattern.matcher(testString).results()
                .map(MatchResult::group)
                .forEach(System.out::println);
    }
}
