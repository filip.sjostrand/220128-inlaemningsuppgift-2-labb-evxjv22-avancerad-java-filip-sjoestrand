package U1_Lönelista;       //Filip Sjöstrand

import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Person> persons = List.of(
                new Person("Gun Larsson", "Woman", 2),
                new Person("Gunvaldina Larsson", "Woman", 4),
                new Person("Gunvald Larsson", "Man", 1.5),
                new Person("Jeff Vader", "Man", 3),
                new Person("Wallace Gromitson", "Man", 5),
                new Person("Kurt-Olof Kurtsson", "Man", 7),
                new Person("Lucky Look", "Man", 9),
                new Person("Steffi Bradbury", "Woman", 6),
                new Person("Johanna von Anka", "Woman", 8),
                new Person("Paris Scandic", "Woman", 10.5)
        );

        //Uppgift 1.1: Snittlönen för kvinnorna respektive männen i listan

        // Kvinnor    (utan summaryStatistics)
        double women = persons.stream()
                .filter(p -> p.getGender().equals("Woman"))
                .mapToDouble(Person::getSalary)
                .average()
                .orElse(0);

        System.out.println(women + " pengar (Kvinnor, snittlön)");

        // Män  (med summaryStatistics)
        double men = persons.stream()
                .filter(p -> p.getGender().equals("Man"))
                .mapToDouble(Person::getSalary)
                .summaryStatistics()
                .getAverage();

        System.out.println(men + " pengar (Män, snittlön)");


        //Uppgift 1.2: Vem som har högst lön totalt
        double max = persons.stream()
                .mapToDouble(Person::getSalary)
                .summaryStatistics()
                .getMax();

        System.out.println(max + " pengar (av alla, högst lön)");


        //Uppgift 1.3: Vem som har lägst lön totalt.double maxSalary = persons.stream()
        double min = persons.stream()
                .mapToDouble(p -> p.getSalary())
                .summaryStatistics()
                .getMin();

        System.out.println(min + " pengar (av alla, lägst lön)");

    }
}
