package U2_Bilfabrik;

// Concrete Product
// ("Factory-side")

public class ConcreteProductBilVWPassat extends ProductBil {
    @Override
    protected void factoryMethod() {
        concretecreator.add(new ConcreteCreatorBilfabrikVW());
        concreteProduct = "ConcreteProductBil: Volkswagen Passat";
    }
}
