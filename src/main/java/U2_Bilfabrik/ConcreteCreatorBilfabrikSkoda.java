package U2_Bilfabrik;

// Concrete Creator
// ("Factory-side")

public class ConcreteCreatorBilfabrikSkoda extends CreatorBilfabrik {
    // Skoda-fabriken tillverkar Skoda-bilar

    @Override
    public String toString() {
        return "BilfabrikSkoda: Tillverkar en Skoda";
    }
    // När vi kör factoryMethod(); ((via Application))
}