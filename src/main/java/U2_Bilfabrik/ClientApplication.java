package U2_Bilfabrik;

// Uppgift 2. Skapa en bilfabrik, med hjälp av factory pattern

import java.util.Scanner;

// Client HMI (Human-Machine Interface), Application
// (Programmet körs härifrån)

public class ClientApplication {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Ange önskat bilmärke: Audi/Skoda/Volkswagen");
        String carBrand = scan.nextLine().toUpperCase();
        System.out.println("Valt bilmärke: " + carBrand + "\n");


        System.out.println("Ange önskad storlek: small/sedan/semicombi/combi");
        String carSize = scan.nextLine().toUpperCase();
        System.out.println("Vald biltyp: " + carSize + "\n");

        ProductBil myNewCar = CreatorBilfabrik.factoryMethod(carBrand, carSize);
        if (myNewCar == null){
            System.out.println("- Aktuellt önskemål: " + "'" + carBrand + " " + carSize + "'" + " ej tillgänglig. Var god välj igen.");
        }
        System.out.println(myNewCar);
    }
}
