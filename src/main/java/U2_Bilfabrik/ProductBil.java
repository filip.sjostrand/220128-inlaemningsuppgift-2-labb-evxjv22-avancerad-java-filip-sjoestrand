package U2_Bilfabrik;

// Product

import java.util.ArrayList;
import java.util.List;

public abstract class ProductBil {

    protected List<CreatorBilfabrik> concretecreator = new ArrayList<>();

    protected String concreteProduct = "";

    protected ProductBil() {
        factoryMethod();
    }

    protected abstract void factoryMethod();

    @Override
    public String toString() {
        return concreteProduct + "\n" + "ProductBil{" +
                "CreatorBilFabrik (extend:as) => " + concretecreator + " " + concreteProduct + " (via factoryMethod();)" +
                '}';
    }

}
