package U2_Bilfabrik;

// Concrete Product

public class ConcreteProductBilVWGolf extends ProductBil {
    @Override
    protected void factoryMethod() {
        concretecreator.add(new ConcreteCreatorBilfabrikVW());
        concreteProduct = "ConcreteProductBil: Volkswagen Golf";
    }
}
