package U2_Bilfabrik;

// Concrete Creator (child - sub-klass)

public class ConcreteCreatorBilfabrikVW extends CreatorBilfabrik {
    // Volkswagen-fabriken tillverkar Volkswagen-bilar

    @Override
    public String toString() {
        return "BilfabrikVolkswagen: Tillverkar en Volkswagen";
    }
    // När vi kör factoryMethod(); ((via Application))
}
