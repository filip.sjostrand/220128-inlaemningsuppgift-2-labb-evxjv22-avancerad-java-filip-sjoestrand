package U2_Bilfabrik;

// Concrete Product

public class ConcreteProductBilSkodaFabia extends ProductBil {
    @Override
    protected void factoryMethod() {
        concretecreator.add(new ConcreteCreatorBilfabrikSkoda());
        concreteProduct = "ConcreteProductBil: Skoda Fabia";
    }
}
