package U2_Bilfabrik;

// Uppgift 2. Skapa en bilfabrik, med hjälp av factory pattern

// Creator
// ("Factory-side")

public abstract class CreatorBilfabrik {
    // Volkswagen Group kan bland annat tillverka bilmärkena: Audi, Skoda och Volkswagen.

    public static ProductBil factoryMethod(String carBrand, String carSize) {

        switch (carBrand + "_" + carSize) {
            case "AUDI_SMALL":
            case "AUDI_SEMICOMBI":
                return new ConcreteProductBilAudiA2();

            case "AUDI_SEDAN":
            case "AUDI_COMBI":
                return new ConcreteProductBilAudiA4();

            case "SKODA_SMALL":
                return new ConcreteProductBilSkodaFabia();

            case "SKODA_SEDAN":
            case "SKODA_COMBI":
                return new ConcreteProductBilSkodaSuperb();

            case "VOLKSWAGEN_SMALL":
            case "VOLKSWAGEN_SEMICOMBI":
                return new ConcreteProductBilVWGolf();

            case "VOLKSWAGEN_SEDAN":
            case "VOLKSWAGEN_COMBI":
                return new ConcreteProductBilVWPassat();

            default:
                return null;
        }
    }

}
