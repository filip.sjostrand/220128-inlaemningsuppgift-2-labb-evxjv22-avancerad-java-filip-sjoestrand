package U2_Bilfabrik;

// Concrete Creator (child - sub-klass)
// ("Factory-side")

public class ConcreteCreatorBilfabrikAudi extends CreatorBilfabrik {
    // Audi-fabriken tillverkar Audi-bilar

    @Override
    public String toString() {
        return "BilfabrikAudi: Tillverkar en Audi";
    }
    // När vi kör factoryMethod(); ((via Application))
}
