package U2_Bilfabrik;

// Concrete Product

public class ConcreteProductBilAudiA4 extends ProductBil {
    @Override
    protected void factoryMethod() {
        concretecreator.add(new ConcreteCreatorBilfabrikAudi());
        concreteProduct = "ConcreteProductBil: Audi A4";
    }
}
